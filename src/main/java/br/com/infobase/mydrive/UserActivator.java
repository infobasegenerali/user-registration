package br.com.infobase.mydrive;

import org.json.JSONObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Calendar;
import java.util.TimeZone;

public class UserActivator {

    private static final String HTTP_ACTION = "POST";
    private static final String SIGNATURE_METHOD = "HMAC-SHA256";
    private static final String ALGORITHM = "HmacSHA256";

    private String url;
    private String routeUri;
    private String fullUrl;

    public UserActivator(String url, String routeUri) {
        this.url = url;
        this.routeUri = routeUri;
        this.fullUrl = this.url + this.routeUri;
    }


    /**
     * Concatenates arrays of bytes together
     *
     * @param byteVars Arrays of bytes to concatenate
     * @return A new array of concatenated bytes
     */
    private static byte[] concatBytes(byte[]... byteVars) {
        int bbSize = 0;


        for (byte[] bytes : byteVars) {
            bbSize += bytes.length;
        }
        ByteBuffer bb = ByteBuffer.allocate(bbSize);
        for (byte[] bytes : byteVars) {
            bb.put(bytes);
        }

        return bb.array();
    }

    /**
     * Encode the data required by the Users api for a request to activate a user
     *
     * @param routeUri        Appended to the host to create the full Url of the Users api
     * @param signatureMethod The HMAC signature method to use
     * @param algorithm       The HMAC algorithm to use
     * @param jsonBodyBytes   Array of bytes containing the JSON body of the request
     * @param secsSinceEpoch  Seconds since the Epoch
     * @param apiKey          Api key of the users api
     * @param secretKey       Secret key of the users api
     * @return The encoded data
     * @throws Exception If an error occurred in the encoding
     */
    private static String getEncodedHash(final String routeUri, final String signatureMethod,
                                         final String algorithm, final byte[] jsonBodyBytes,
                                         final long secsSinceEpoch, final String apiKey,
                                         final String secretKey) throws Exception {
        final String colon = ":";
        final byte[] colBytes = colon.getBytes();
        byte[] unsignedTokenBytes = concatBytes(HTTP_ACTION.getBytes(),
                colBytes, routeUri.getBytes(),
                colBytes, signatureMethod.getBytes(),
                colBytes, apiKey.getBytes(),
                colBytes, ("" + secsSinceEpoch).getBytes(),
                colBytes, jsonBodyBytes);
        return encode(algorithm, secretKey, unsignedTokenBytes);
    }

    /**
     * Base 64 encode an array of bytes given the provided algorithm and encoded key.
     *
     * @param algorithm          The algorithm to use in the encoding
     * @param encodedKey         A previously encoded key to use in the Base 64 encoding
     * @param unsignedTokenBytes Array of unsigned data
     * @return Encoded String of the data
     * @throws Exception If an error occurs in the encoding
     */
    private static String encode(String algorithm, String encodedKey,
                                 byte[] unsignedTokenBytes) throws Exception {
        final Mac mac = Mac.getInstance(algorithm);

        byte[] decodedBytes = Base64.getDecoder().decode(encodedKey);

        final SecretKeySpec secretKey = new SecretKeySpec(decodedBytes, algorithm);
        mac.init(secretKey);
        final byte[] macData = mac.doFinal(unsignedTokenBytes);

        //HEX encode
        String encoded = "";
        for (final byte element : macData) {
            encoded += Integer.toString((element & 0xff) + 0x100, 16).
                    substring(1);
        }
        return encoded;
    }

    /**
     * Calculate the number of seconds since 1970
     *
     * @return The number of seconds since 1970
     */
    private static long getSecondsSinceEpoch() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        return calendar.getTimeInMillis() / 1000L;
    }

    /**
     * Activate a user for the Client Id configuration of credentials.
     *
     * @param clientId  Identifier for the user to activate
     * @param appToken  App Token of the app
     * @param apiKey    Api key of the admin user
     * @param secretKey Secret key of the admin user
     * @return The Pin Code for the new user
     * @throws Exception If an error occurs in the Users api
     */
    public String activateUser(final String clientId, final String appToken, String apiKey,
                               final String secretKey) throws Exception {

        System.out.println("Activate user with id: " + clientId);

        // app:
        JSONObject appJson = new JSONObject();
        appJson.put("version", "1.1.1.1");

        // device:
        JSONObject deviceJson = new JSONObject();
        deviceJson.put("os", "example_os");
        deviceJson.put("os_version", "1.1.1");
        deviceJson.put("name", "example_name");
        deviceJson.put("model", "example_model");

        // registration_data:
        JSONObject registrationDataJson = new JSONObject();
        registrationDataJson.put("device", deviceJson);
        registrationDataJson.put("app", appJson);

        // user:
        JSONObject userJson = new JSONObject();
        userJson.put("client_id", clientId);
        userJson.put("customer_group", "example_group");
        userJson.put("registration_data", registrationDataJson);

        // final JSON
        JSONObject activateUserJson = new JSONObject();
        activateUserJson.put("user", userJson);

        final byte[] jsonBodyBytes = activateUserJson.toString().getBytes();
        HTTPJSONRequest jsonRequest = new HTTPJSONRequest(fullUrl, apiKey);
        final long secsSinceEpoch = getSecondsSinceEpoch();

        System.out.println("POSTing to: " + jsonRequest.getUrl());
        System.out.println("Body: " + activateUserJson.toString());
        int statusCode = jsonRequest.post(jsonBodyBytes, appToken, apiKey, secsSinceEpoch, getEncodedHash(routeUri,
                SIGNATURE_METHOD, ALGORITHM, jsonBodyBytes, secsSinceEpoch, apiKey, secretKey));

        boolean errorOccurred = false;
        String subscriptionId = "";
        if (statusCode >= HTTPJSONRequest.HTTP_STATUS_CODE_MIN_OK &&
                statusCode <= HTTPJSONRequest.HTTP_STATUS_CODE_MAX_OK) {

            // Decode the response JSON data
            ActivateUserReceivedJSON activateUserReceivedJSONData = jsonRequest.decodeResponseAs(ActivateUserReceivedJSON.class);
            subscriptionId = activateUserReceivedJSONData.subscription_id;
            System.out.println("Subscription Id: " + subscriptionId);
            System.out.println("Authorization Token: " + activateUserReceivedJSONData.authorization_token);

        } else if (statusCode == HTTPJSONRequest.HTTP_STATUS_CODE_COULD_NOT_AUTH) {
            System.err.println("--- Make sure the correct admin user’s keys are used --- ");
            errorOccurred = true;
        } else {
            errorOccurred = true;
        }

        if (errorOccurred) {
            ErrorReturnData errorReturnData = getErrorReturnData(
                    jsonRequest);
            int secondaryCode = errorReturnData.getCode();
            final String message = errorReturnData.getMessage();
            System.err.println("secondaryCode: " + secondaryCode);
            System.err.println("message: " + message);
        }

        return subscriptionId;

    }

    // -- JSON response structures ----------------------------------------------------------------

    /**
     * Represents a JSON response from the Users api
     */
    public static class ActivateUserReceivedJSON {
        public String subscription_id;
        public String authorization_token;
        public DeviceConfigurationReceivedJSON device_configuration;
    }

    /**
     * Represents the device configuration from a JSON response, sent by
     * the Users api
     */
    public static class DeviceConfigurationReceivedJSON {
        public String mydrive_timeout_stop;
        public String mydrive_timeout_start;
        public String mydrive_speed_start;
        public String mydrive_speed_stop;
        public String mydrive_trip_url;
        public String mydrive_trip_key;
        public String mydrive_host_page;
        public int autostart_duration_s;
        public int autostart_period_s;
    }

    /*****************************************************
     * Attempts to retrieve error information from a JSON string.
     *****************************************************/
    public static ErrorReturnData getErrorReturnData(HTTPJSONRequest jsonRequest) throws IOException {

        // Get the response body content as a string
        String responseBodyContentString = jsonRequest.getResponseAsString();
        if (responseBodyContentString == null) {
            System.err.println("Could not decode error return data:noresponse body");
            return (null);
        }
        // Try decoding the JSON response into the latest version o error data
        try {
            ErrorCodeJSONFormat errorJSONData2 = jsonRequest.
                    decodeResponseAs(responseBodyContentString,
                            ErrorCodeJSONFormat.class);
            if (errorJSONData2 != null)
                return (new ErrorReturnData(errorJSONData2.error, errorJSONData2.code));
        } catch (Exception e) {
            // Different error type, carry on below
        }
        // If that fails, try decoding the JSON response into the older version of error data
        try {
            ErrorJSONFormat errorJSONData1 = jsonRequest
                    .decodeResponseAs(responseBodyContentString, ErrorJSONFormat.class);
            if (errorJSONData1 != null)
                return (new ErrorReturnData(errorJSONData1.error, -1));
        } catch (Exception e) {
            // Different error type, carry on below
        }

        // Neither format worked
        return (new ErrorReturnData("Unknown Error", -1));
    }

    /*****************************************************
     * Error data v1.
     *****************************************************/
    private static class ErrorJSONFormat {
        public String error;
    }

    /*****************************************************
     * Error data v2.
     *****************************************************/
    private static class ErrorCodeJSONFormat {
        public String error;
        public int code;
    }

    /*****************************************************
     * Error data.
     *****************************************************/
    public static class ErrorReturnData {
        private String mMessage;
        private int mCode;

        private ErrorReturnData(String message, int code) {
            mMessage = message;
            mCode = code;
        }

        public String getMessage() {
            return (mMessage);
        }

        public int getCode() {
            return (mCode);
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Code:");
            builder.append(mCode);
            builder.append(" Message:");
            builder.append(mMessage);
            return builder.toString();
        }

    }

}
