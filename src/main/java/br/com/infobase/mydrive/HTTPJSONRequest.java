package br.com.infobase.mydrive;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

class HTTPJSONRequest {
    private static final String HTTP_HEADER_APP_TOKEN = "X-MyDrive-App-Token";
    private static final String HTTP_HEADER_AUTHORISATION = "Authorization";
    private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    private static final String MIME_TYPE_JSON = "application/json";

    public static final int HTTP_STATUS_CODE_MIN_OK = 200;
    public static final int HTTP_STATUS_CODE_CREATED = 201;
    public static final int HTTP_STATUS_CODE_MAX_OK = 299;
    public static final int HTTP_STATUS_CODE_BAD_REQUEST = 400;
    public static final int HTTP_STATUS_CODE_COULD_NOT_AUTH = 401;

    private String mURL;
    private String mAuthKey;
    private HttpClient mHTTPClient;
    private HttpPost mHTTPPost;
    private HttpPut mHTTPPut;
    private HttpResponse mHTTPResponse;
    private String mResponseAsString;

    public HTTPJSONRequest(String url, String authKey) {
        setUp(url);
        this.mAuthKey = authKey;
    }

    ////////// Method(s) //////////

    /*****************************************************
     * Initialises the request.
     *****************************************************/
    private void setUp(String url) {
        this.mURL = url;
        this.mHTTPClient = new DefaultHttpClient();
        this.mHTTPPost = new HttpPost(url);
        this.mHTTPPut = new HttpPut(url);
    }

    /*****************************************************
     * Returns the URL.
     *****************************************************/
    public String getUrl() {
        return (mURL);
    }

    /*****************************************************
     * Performs a POST request with a secret key.
     *****************************************************/
    public int post(byte[] bodyBytes, String appToken, final String
            apiKey, final long secondsSinceEpoch, final String base64Hmac)
            throws Exception {
        attachCommonHeadersWithBase64Hmac(mHTTPPost, "", appToken,
                apiKey, secondsSinceEpoch, base64Hmac);
        attachPostEntity(bodyBytes);
        mResponseAsString = null;
        return (execute(mHTTPPost));
    }

    /*****************************************************************
     * Attaches common headers to an HTTP request with a secret key.
     *****************************************************************/
    private void attachCommonHeadersWithBase64Hmac(HttpUriRequest httpURIRequest, String md5String,
                                                   String appToken, String apiKey,
                                                   long secondsSinceEpoch, String base64Hmac) {
        System.out.println("-- CREDENTIALS ---");
        System.out.println("appToken:" + appToken);
        System.out.println("apiKey:" + apiKey);
        System.out.println("secondsSinceEpoch:" + secondsSinceEpoch);
        System.out.println("base64Hmac:" + base64Hmac);

        System.out.println("-- HEADERS ---");
        httpURIRequest.setHeader(HTTP_HEADER_APP_TOKEN, appToken);
        System.out.println(HTTP_HEADER_APP_TOKEN + ": " + appToken);
        if (!base64Hmac.isEmpty()) {
            String auth = "MyDriveHMAC HMAC - SHA256:" + apiKey + ":" + secondsSinceEpoch + ":" + base64Hmac;
            httpURIRequest.setHeader(HTTP_HEADER_AUTHORISATION,
                    auth);
            System.out.println(HTTP_HEADER_AUTHORISATION + ": " + auth);
        }
        httpURIRequest.setHeader(HTTP_HEADER_CONTENT_TYPE, MIME_TYPE_JSON);
        System.out.println(HTTP_HEADER_CONTENT_TYPE + ": " + MIME_TYPE_JSON);
    }

    /*****************************************************
     * Attaches the supplied byte data to the POST request.
     *****************************************************/
    private void attachPostEntity(byte[] body) {
        ByteArrayEntity httpEntity = new ByteArrayEntity(body);
        httpEntity.setContentType(MIME_TYPE_JSON);
        httpEntity.setChunked(false); // Don’t do chunked encoding!
        mHTTPPost.setEntity(httpEntity);
    }

    /*****************************************************
     * Performs an HTTP request.
     *****************************************************/
    private int execute(HttpUriRequest httpURIRequest) throws ClientProtocolException, IOException {
        mHTTPResponse = mHTTPClient.execute(httpURIRequest);
        StatusLine statusLine = mHTTPResponse.getStatusLine();
        int statusCode = statusLine.getStatusCode();
        System.out.println("HTTP status code = " + statusCode);
        return statusCode;
    }

    /*****************************************************
     * Returns the actual HTTP response.
     *****************************************************/
    public HttpResponse getResponse() {
        return (mHTTPResponse);
    }

    /*****************************************************
     * Returns the response body content as a string.
     *****************************************************/
    public String getResponseAsString() throws IOException {
        // If we haven’t already retrieved the response as a string - do so now
        if (mResponseAsString == null) {
            String responseAsString = "<no response>";
            HttpResponse httpResponse = getResponse();
            if (httpResponse != null) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) responseAsString = streamToString(httpEntity.getContent());
            }
            mResponseAsString = responseAsString;
        }
        return (mResponseAsString);
    }


    /*****************************************************
     * Reads the supplied input stream, and returns the data
     * as a string.
     *****************************************************/
    private String streamToString(InputStream inputStream) throws IOException {
        if (inputStream == null) return ("<null input stream>");
        StringWriter stringWriter = new StringWriter();
        IOUtils.copy(inputStream, stringWriter);
        return (stringWriter.toString());
    }

    /*****************************************************
     * Attempts to decode the supplied JSON response string
     * into the supplied data type, where the names of the
     * class fields correspond to the JSON structure.
     *****************************************************/
    public <E> E decodeResponseAs(String jsonResponseString, Class<E> jsonDataClass)
            throws JsonSyntaxException, IllegalStateException,
            IOException {
        if (jsonResponseString == null) {
            System.out.println("Unable to decode JSON response:no response string");
            return (null);
        }
        if (mHTTPResponse == null) {

            System.out.println("Unable to decode JSON response: no HTTP response");
            return (null);
        }
        return new Gson().fromJson(jsonResponseString, jsonDataClass);
    }

    /*****************************************************
     * Attempts to decode the JSON response into the supplied
     * data type, where the names of the class fields correspond
     * to the JSON structure.
     *****************************************************/
    public <E> E decodeResponseAs(Class<E> jsonDataClass)
            throws JsonSyntaxException, IllegalStateException, IOException {
        return (decodeResponseAs(getResponseAsString(), jsonDataClass));
    }

}
