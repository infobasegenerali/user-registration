package br.com.infobase.mydrive;

import java.util.Calendar;

public class HMACActivateUserExample {

    private static final String APP_ID = "38";
    private static final String HOST = "https://generali-brazil-development-app.mydrivesolutions.net";
    private static final String ROUTE_URI = "/v2/apps/" + APP_ID + "/users/activate";
    private static final String APP_TOKEN = "a35c31fc9f4f2adf6daed6ea624e64db7752580e";
    private static final String API_KEY = "-y8XtM_Qua9pgZzV4v6P";
    private static final String SECRET_KEY = "d6JPWv0mQvPZM1ccbp9HAOzo2hrrW46Aob8Mci9oC14un55H8RP5SlyRlpI3hvWXiC/EQwtnJmXqmuqOnE6p1A==";

    public static void main(String[] args) {
        try {
            UserActivator userActivator = new UserActivator(HOST, ROUTE_URI);
            final long uniqueIndex = Calendar.getInstance().getTimeInMillis();
            final String clientId = "example" + uniqueIndex;
            userActivator.activateUser(clientId, APP_TOKEN, API_KEY, SECRET_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
